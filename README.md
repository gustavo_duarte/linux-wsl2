# WSL2 + Ubuntu 20.04 + Mate

Este proyecto habilita WSL2, instala Ubuntu 20.04 y configura Mate como entorno grafico.

Esta basado en el proyecto "Kali-xRDP (https://github.com/DesktopECHO/Kali-xRDP), WSL2 setup (https://github.com/Layer8Err/WSL2setup) y esta guía https://levelup.gitconnected.com/install-ubuntu-desktop-gui-in-wsl2-7c3730e33bb2.


**INSTRUCCIONES:  Abrir un command prompt de Windows con permisos de Administrador, copie y pegue el siguiente comando:**

## Mate WSL2
    iex ((New-Object System.Net.WebClient).DownloadString('https://bitbucket.org/gustavo_duarte/linux-wsl2/raw/develop/install.ps1'))
