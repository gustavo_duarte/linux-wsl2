


if ((Get-Content -Path "$env:TEMP\status.TMP" 2> $null) -eq "reboot") {
	# Reboot already done
	Remove-Item -Path "$env:TEMP\reboot.TMP" -Force 2> $null
	Remove-Item -Path "$env:TEMP\status.TMP" -Force 2> $null
	
	Write-Host ""
	Write-Host "#### Coontniue instalacion de Ubuntu ####"
	Write-Host ""
	Write-Host "Doing something 3..."
	Start-Sleep -s 2
} else {
	Write-Host ""
	Write-Host "#### Inicio instalacion de Ubuntu ####"
	Write-Host ""
	Write-Host "Doing something 1..."
	Start-Sleep -s 2

	Write-Host ""
	Write-Host "Doing something 2..."
	Start-Sleep -s 2

	Write-Host ""
	Write-Host "Check if reboot is needed..."

	if (Test-Path "$env:TEMP\reboot.TMP"){
		Write-Host "Reboot is needed."
		Write-Output 'reboot' > $env:TEMP\status.TMP;
		Exit
	} else {
		Write-Host "Reboot is NOT needed."
		Write-Host ""
		Write-Host "Doing something 3..."
		Start-Sleep -s 2
	}
}


Write-Host ""
Write-Host "#### Fin instalacion de Ubuntu ####"