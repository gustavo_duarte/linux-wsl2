
. "$PSScriptRoot\Utils.ps1"

### STATUS ubuntuInstalled ###
$result = wsl -d "Ubuntu-20.04" -e uname
if ($result -Match "Linux") {
  Save-Status([StatusType]::ubuntuInstalled)
} else {
  Log -Msg "Algo salio mal en la instalacion de Ubuntu Appx" -Level "Error"
  Exit
}

Log -Msg "## Incio de instalacion del entorno grafico ##"

### STATUS chcolateyInstalled ###
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072;
if (Get-Status -lt [StatusType]::chocolateyInstalled.value__) {
  Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1')) *>&1| Log -Msg "..Instalando Chocolatey"
  $env:PATH += ";$env:PROGRAMDATA\chocolatey\bin"
  Save-Status([StatusType]::chocolateyInstalled)
}

### STATUS runtimeInstalled ###
if (Get-Status -lt [StatusType]::runtimeInstalled.value__) {
  choco install dotnet-5.0-runtime -y *>&1 | Log -Msg "..Instalando .NET 5.0 Runtime"
  Save-Status([StatusType]::runtimeInstalled)
} else {
  Log -Msg ".....NET 5.0 Runtime ya instalado"
}

### STATUS xsrvInstalled ###
if (Get-Status -lt [StatusType]::xsrvInstalled.value__) {
  choco install vcxsrv -y *>&1 | Log -Msg "..Instalando Xserver"
  Save-Status([StatusType]::xsrvInstalled)
} else {
  Log -Msg "....Xserver ya instalado"
}



Copy-Item ".\linux_cmds.sh" -Destination "$env:userprofile\.ubuntu\linux_cmds.sh" *>&1 | Log -Msg "."
Copy-Item ".\01_reload_vcxsrv.ps1" -Destination "$env:userprofile\.ubuntu\01_reload_vcxsrv.ps1" *>&1 | Log -Msg "."
Copy-Item ".\02_start_mate.sh" -Destination "$env:userprofile\.ubuntu\02_start_mate.sh" *>&1 | Log -Msg "."
Copy-Item ".\03_start_mate.vbs" -Destination "$env:userprofile\.ubuntu\03_start_mate.vbs" *>&1 | Log -Msg "."
Copy-Item ".\ubuntu.ico" -Destination "$env:userprofile\.ubuntu\ubuntu.ico" *>&1 | Log -Msg "."


### STATUS ubuntuUpdated
Log -Msg "....Actualizando paquetes de Ubuntu"
wsl sh -c "/mnt/c/users/$env:userName/.ubuntu/linux_cmds.sh update_pkgs"
# if ($result -Match "ERROR") {
#   Log -Msg "$result" -Level "Error"
#   Exit
# } else {
#   Save-Status([StatusType]::ubuntuUpdated)
# }
Save-Status([StatusType]::ubuntuUpdated)
### STATUS ubuntuDesktopInstalled ###
Log -Msg "....Instalando entorno grafico"
wsl sh -c "/mnt/c/users/$env:userName/.ubuntu/linux_cmds.sh install_gui"
# if ($result -Match "ERROR") {
#   Log -Msg "$result" -Level "Error"
#   Exit
# } else {
#   Save-Status([StatusType]::ubuntuDesktopInstalled)
# }
Save-Status([StatusType]::ubuntuDesktopInstalled)

Log -Msg "....Creando usuario Ceibal"
$result=wsl sh -c "/mnt/c/users/$env:userName/.ubuntu/linux_cmds.sh user_ceibal"
if ($result -Match "ERROR") {
  Log -Msg "$result" -Level "Error"
  Exit
}

$result=wsl sh -c "/mnt/c/users/$env:userName/.ubuntu/linux_cmds.sh set_username"
if ($result -Match "ERROR") {
  Log -Msg "$result" -Level "Error"
  Exit
}

Ubuntu2004.exe config --default-user ceibal  | Log -Msg "Configuro ceibal como usuario por defecto ..."


$shortcut_location = "$env:userprofile\Desktop\Ubuntu.lnk"
$program_location = "$env:userprofile\.ubuntu\03_start_mate.vbs"
$icon_location = "$env:userprofile\.ubuntu\ubuntu.ico"

$object = new-object -comobject wscript.shell
$shortcut = $object.createshortcut($shortcut_location)
$shortcut.targetpath = $program_location
$shortcut.iconlocation = $icon_location
$shortcut.save() *>&1 | Log -Msg "....Acceso directo creado"

New-NetFirewallRule -DisplayName "Xserver" -Direction Inbound -Program "$env:ProgramFiles\VcXsrv\vcxsrv.exe" -Action Allow *>&1| Log -Msg "....Agrego regla en Firewall"

wsl --shutdown *>&1 | Log -Msg "Reinicio wsl"

### STATUS complete ###
Save-Status([StatusType]::complete)
Log -Msg "## Fin de la configuracion del entorno grafico ##"

