$script:build="https://bitbucket.org/gustavo_duarte/linux-wsl2/get/develop.zip"

Write-Host ""
Write-Host ""
Write-Host "#### Inicio instalacion de Ubuntu ####"

Remove-Item -Path "$env:TEMP\*linux-wsl2*" -Force -Recurse

Write-Host "..Descargo instalador"
Start-BitsTransfer -Source $script:build -Destination "$env:TEMP\build.zip"

Write-Host "..Descomprimo instalador"
Expand-Archive -Path "$env:TEMP\build.zip" -DestinationPath "$env:TEMP" -Force *>&1 | Write-Output

$initialPath = Get-Location
Set-Location -Path "$env:TEMP\*linux-wsl2*"


.\SetupWSL2.ps1

Set-Location -Path $initialPath
