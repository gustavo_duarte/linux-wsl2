$Script:applicationName = "Instalador Linux WSL2"
$script:applicationPath = "$env:LOCALAPPDATA\$applicationName"
$Script:statusFilePath = "$applicationPath\status.TMP"
$Script:rebootFilePath = "$env:TEMP\reboot.TMP"

enum StatusType {
	init
	wsl2Enabled
	virtualPlatformEnabled
	kernelUpdated
  chcolateyInstalled
  runtimeInstalled
  xsrvInstalled
	ubuntuInstalled
  ubuntuUpdated
	ubuntuDesktopInstalled
  complete
}

function New-Environment () {
  New-Item -Path $env:LOCALAPPDATA -Name $Script:applicationName -ItemType "directory" *> $null

  if ((Test-Path  -Path $Script:statusFilePath) -eq $false) {
    New-Item -Path $script:applicationPath -Name "status.TMP" -ItemType "file" *> $null
    Save-Status([StatusType]::init)
  }

  New-Item -Path $env:userprofile -Name ".ubuntu" -ItemType "directory" *> $null

  New-EventLog -LogName Application -Source $Script:applicationName *> $null

}


function New-Reboot () {
  New-Item -Path $env:TEMP -Name "reboot.TMP" -ItemType "file" 2> $null
}
function Remove-Reboot () {
  Remove-Item -Path $Script:rebootFilePath -Force 2> $null
}
function Save-Status ($status) {
  if ($status.value__ -gt (Get-Status)) {
    Write-Output $status.value__ > $Script:statusFilePath
  }
}

function Get-Status () {
  $status=Get-Content -Path $Script:statusFilePath -TotalCount 1
  return $status -as [Int]
}

function Log {
  Param (
    [Parameter(Mandatory=$true)][String] $Msg,
    [Parameter(ValueFromPipeline)][String] $RawData,
    [Parameter(Mandatory=$false)][String] $Level="Information",
    [Parameter(Mandatory=$false)][bool] $Console=$true
  )

  if ($Console) {
    if ($Level -eq "Error") {
      Write-Host $Msg -ForegroundColor Red
    } else  {
      Write-Host $Msg
    }
  }
  Write-EventLog -LogName Application -Source $Script:applicationName -EventID 1 -EntryType $Level -Message "$Msg `r`n $RawData"
}
