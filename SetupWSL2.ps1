# Install WSL
# This script needs to be run as a priviledged user
. "$PSScriptRoot\Utils.ps1"

New-Environment

# Borro archivo de reboot, si existe.
Remove-Reboot

function Update-Kernel () {
    Log -Msg "..Descargando actualizacion del kernal WSL2"
    $kernelURI = 'https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi'
    $kernelUpdate = ((Get-Location).Path) + '\wsl_update_x64.msi'
    (New-Object System.Net.WebClient).DownloadFile($kernelURI, $kernelUpdate)
    Log -Msg "....Instalando actualizacion del kernel WSL2"
    msiexec /i $kernelUpdate /qn
    Start-Sleep -Seconds 5
    Save-Status ([StatusType]::kernelUpdated)
}

function Get-Kernel-Updated () {
    # Check for Kernel Update Package
    #
    Log -Msg "Checking for Windows Subsystem for Linux Update..."
    $uninstall64 = Get-ChildItem "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall" | ForEach-Object { Get-ItemProperty $_.PSPath } | Select-Object DisplayName, Publisher, DisplayVersion, InstallDate
    if ($uninstall64.DisplayName -contains 'Windows Subsystem for Linux Update') {
        return $true
    } else {
        return $false
    }
}

#$pkgs = (Get-AppxPackage).Name

# function Get-WSLlist {
    # $wslinstalls = New-Object Collections.Generic.List[String]
    # $(wsl -l) | ForEach-Object { if ($_.Length -gt 1){ $wslinstalls.Add($_) } }
    # $wslinstalls = $wslinstalls | Where-Object { $_ -ne 'Windows Subsystem for Linux Distributions:' }
    # return $wslinstalls
# }

 # function Get-WSLExistance ($distro) {
    ##Check for the existence of a distro
    #return Installed as Bool
    # $wslImport = $false
    # if (($distro.AppxName).Length -eq 0){ $wslImport = $true }
    # $installed = $false
    # if ( $wslImport -eq $false ){
        # if ($pkgs -match $distro.AppxName) {
            # $installed = $true
        # }
    # } else {
        # if (Get-WSLlist -contains ($distro.Name).Replace("-", " ")){
            # $installed = $true
        # }
    # }
    # return $installed
# }

function Select-Distro () {
    # See: https://docs.microsoft.com/en-us/windows/wsl/install-manual
    # You can also use https://store.rg-adguard.net to get Appx links from Windows Store links
    $distrolist = (
        [PSCustomObject]@{
            'Name' = 'Ubuntu 20.04'
            'URI' = 'https://aka.ms/wslubuntu2004'
            'AppxName' = 'CanonicalGroupLimited.Ubuntu20.04onWindows'
            'winpe' = 'ubuntu2004.exe'
            'installed' = $false
        },
        [PSCustomObject]@{
            'Name' = 'Ubuntu 18.04'
            'URI' = 'https://aka.ms/wsl-ubuntu-1804'
            'AppxName' = 'CanonicalGroupLimited.Ubuntu18.04onWindows'
            'winpe' = 'ubuntu1804.exe'
            'installed' = $false
        },
        [PSCustomObject]@{
            'Name' = 'Ubuntu 16.04'
            'URI' = 'https://aka.ms/wsl-ubuntu-1604'
            'AppxName' = 'CanonicalGroupLimited.Ubuntu16.04onWindows'
            'winpe' = 'ubuntu1604.exe'
            'installed' = $false
        },
        [PSCustomObject]@{
            'Name' = 'Debian'
            'URI' = 'https://aka.ms/wsl-debian-gnulinux'
            'AppxName' = 'TheDebianProject.DebianGNULinux'
            'winpe' = 'debian.exe'
            'installed' = $false
        },
        [PSCustomObject]@{
            'Name' = 'Kali'
            'URI' = 'https://aka.ms/wsl-kali-linux-new'
            'AppxName' = 'KaliLinux'
            'winpe' = 'kali.exe'
            'installed' = $false
        },
        [PSCustomObject]@{
            'Name' = 'openSUSE Leap 42'
            'URI' = 'https://aka.ms/wsl-opensuse-42'
            'AppxName' = 'openSUSELeap42'
            'winpe' = 'openSUSE-42.exe'
            'installed' = $false
        },
        [PSCustomObject]@{
            'Name' = 'openSUSE Leap 15.2'
            'URI' = 'https://aka.ms/wsl-opensuseleap15-2'
            'AppxName' = 'openSUSELeap15'
            'winpe' = 'openSUSE-Leap-15.2.exe'
            'installed' = $false
        },
        [PSCustomObject]@{
            'Name' = 'SUSE Linux Enterprise Server 12'
            'URI' = 'https://aka.ms/wsl-sles-12'
            'AppxName' = 'SUSELinuxEnterpriseServer12'
            'winpe' = 'SLES-12.exe'
            'installed' = $false
        },
        [PSCustomObject]@{
            'Name' = 'SUSE Linux Enterprise Server 15'
            'URI' = 'https://aka.ms/wsl-SUSELinuxEnterpriseServer15SP2'
            'AppxName' = 'SUSELinuxEnterpriseServer15SP2'
            'winpe' = 'SUSE-Linux-Enterprise-Server-15-SP2.exe'
            'installed' = $false
        },
        [PSCustomObject]@{
            'Name' = 'Alpine'
            'StoreLink' = 'https://www.microsoft.com/en-us/p/alpine-wsl/9p804crf0395'
            'URI' = ''
            'AppxName' = 'AlpineWSL'
            'winpe' = 'Alpine.exe'
            'installed' = $false
        },
        [PSCustomObject]@{
            'Name' = 'Fedora Remix for WSL'
            'URI' = 'https://github.com/WhitewaterFoundry/Fedora-Remix-for-WSL/releases/download/31.5.0/Fedora-Remix-for-WSL_31.5.0.0_x64_arm64.appxbundle'
            'AppxName' = 'FedoraRemix'
            'winpe' = 'fedoraremix.exe'
            'installed' = $false
            'sideloadreqd' = $true
        }
    )
    #$distrolist | ForEach-Object { $_.installed = Get-WSLExistance($_) }
    # Retorno solo Ubuntu 20.04
    $choice = $distrolist[0]
    return $choice
}

function Install-Distro ($distro) {

    function Import-WSL ($distro) {
        Log -Msg "....Inicio descarga de la distro $($distro.Name)"
		    $distroinstall = "$env:LOCALAPPDATA\lxss"
        $wslname = $($distro.Name).Replace(" ", "-")
        $Filename = "$env:temp\" + $wslname + ".rootfs.tar.gz"
        $client = New-Object net.WebClient
        $client.DownloadFile($distro.URI, $Filename)
        if (Test-Path $Filename){
            Log -Msg "....Importando $($distro.Name)"
            wsl.exe --import $wslname $distroinstall $Filename
        } else {
            Log -Msg "No se puede instalar $($distro.Name) missing rootfs.tar.gz package. (Download failed)" -Level "Error" -Console $true
        }
    }
    function Add-WSLAppx ($distro) {
        $Filename = "$env:temp\" + "$($distro.AppxName).appx"
        Log -Msg "....Inicio descarga de la distro $($distro.Name)"
        $client = New-Object net.WebClient
        $client.DownloadFile($distro.URI, $Filename)
        if (Test-Path $Filename) {
            Log -Msg "....Agregando la distro $($distro.Name)"
            Add-AppxPackage -Path $Filename
        } else {
            Log -Msg "Cannot install $($distro.Name) missing Appx package. (Download failed)" -Level "Error" -Console $true
        }
        Start-Sleep -Seconds 5
    }

    Add-WSLAppx($distro)

    # if (Get-WSLExistance($distro)) {
    #     Log -Msg "Se encontro la Distro..."
    # } else {
    #     if ($($distro.AppxName).Length -gt 1){
    #         Add-WSLAppx($distro)
    #     } else {
    #         Import-WSL($distro)
    #     }
    # }
}


#############################################################################################################

### STATUS wsl2enabled ###
Log -Msg "..Inicio configuracion de WSL2"
$rebootRequired = $false
if ((Get-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux).State -ne 'Enabled'){
    Log -Msg "....Habilitando WSL2"
    $wslinst = Enable-WindowsOptionalFeature -Online -NoRestart -FeatureName Microsoft-Windows-Subsystem-Linux
    if ($wslinst.Restartneeded -eq $true){
      New-Reboot
      Save-Status ([StatusType]::wsl2Enabled)
      $rebootRequired = $true
    }
} else {
    Log -Msg "....WSL2 ya habilitado"
    Save-Status ([StatusType]::wsl2Enabled)
}

### STATUS virtualPlatformEnabled ###
Log -Msg "..Verificando Plataforma de Maquina Virtual"
if ((Get-WindowsOptionalFeature -Online -FeatureName VirtualMachinePlatform).State -ne 'Enabled'){
    Log -Msg "....Habilitando Plataforma de Maquina Virtual"
    $vmpinst = Enable-WindowsOptionalFeature -Online -NoRestart -FeatureName VirtualMachinePlatform
    if ($vmpinst.RestartNeeded -eq $true){
      New-Reboot
      Log -Msg "....Plataforma de Maquina Virtual habilitada"
      Save-Status ([StatusType]::virtualPlatformEnabled)
      $rebootRequired = $true
    }
} else {
    Log -Msg "....Platforma de Maquina Virtual ya habilitada"
    Save-Status ([StatusType]::virtualPlatformEnabled)
}

if ($rebootRequired) {
    Log -Msg "!!! Es neceario reiniciar el PC para continuar la instalacion !!!!"
    Exit
}

### STATUS kernelUpdated ###

if ((Get-Status) -lt [StatusType]::kernelUpdated.value__) {
  Update-Kernel
}

Log -Msg "....Configuro version 2 de WSL2 por defecto"
wsl --set-default-version 2 *>$null
Log -Msg "..Fin de configuracion WSL2"


### STATUS ubuntuInstalled ###
if ((Get-Status) -lt [StatusType]::ubuntuInstalled.value__) {
  Log -Msg "..Instalando Ubuntu Appx"
  $distro = Select-Distro
  Install-Distro($distro)

  if ($distro.AppxName.Length -gt 1) {
    Log -Msg "....Instalando la Appx"
    Start-Process $distro.winpe -ArgumentList "install --root" -Wait
    Log -Msg "..Fin de instalacion de Ubuntu Appx"
  } else {
    Log -Msg "No se encontro la ditribucion Linux" -Level "Error"
  }
  } else {
  Log -Msg "..Ubuntu Appx ya instalado"
}

### STATUS ubuntuInstalled ###
$result = wsl -d "Ubuntu-20.04" -e uname
if ($result -Match "Linux") {
  Save-Status([StatusType]::ubuntuInstalled)
} else {
  Log -Msg "Algo salio mal en la instalacion de Ubuntu Appx" -Level "Error"
  Exit
}

Log -Msg "## Incio de instalacion del entorno grafico ##"

### STATUS chcolateyInstalled ###
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072;
if (Get-Status -lt [StatusType]::chocolateyInstalled.value__) {
  Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1')) *>&1| Log -Msg "..Instalando Chocolatey"
  $env:PATH += ";$env:PROGRAMDATA\chocolatey\bin"
  Save-Status([StatusType]::chocolateyInstalled)
}

### STATUS runtimeInstalled ###
if (Get-Status -lt [StatusType]::runtimeInstalled.value__) {
  choco install dotnet-5.0-runtime -y *>&1 | Log -Msg "..Instalando .NET 5.0 Runtime"
  Save-Status([StatusType]::runtimeInstalled)
} else {
  Log -Msg ".....NET 5.0 Runtime ya instalado"
}

### STATUS xsrvInstalled ###
if (Get-Status -lt [StatusType]::xsrvInstalled.value__) {
  choco install vcxsrv -y *>&1 | Log -Msg "..Instalando Xserver"
  Save-Status([StatusType]::xsrvInstalled)
} else {
  Log -Msg "....Xserver ya instalado"
}



Copy-Item ".\linux_cmds.sh" -Destination "$env:userprofile\.ubuntu\linux_cmds.sh" *>&1 | Log -Msg "."
Copy-Item ".\01_reload_vcxsrv.ps1" -Destination "$env:userprofile\.ubuntu\01_reload_vcxsrv.ps1" *>&1 | Log -Msg "."
Copy-Item ".\02_start_mate.sh" -Destination "$env:userprofile\.ubuntu\02_start_mate.sh" *>&1 | Log -Msg "."
Copy-Item ".\03_start_mate.vbs" -Destination "$env:userprofile\.ubuntu\03_start_mate.vbs" *>&1 | Log -Msg "."
Copy-Item ".\ubuntu.ico" -Destination "$env:userprofile\.ubuntu\ubuntu.ico" *>&1 | Log -Msg "."


### STATUS ubuntuUpdated
Log -Msg "....Actualizando paquetes de Ubuntu"
wsl sh -c "/mnt/c/users/$env:userName/.ubuntu/linux_cmds.sh update_pkgs"
# if ($result -Match "ERROR") {
#   Log -Msg "$result" -Level "Error"
#   Exit
# } else {
#   Save-Status([StatusType]::ubuntuUpdated)
# }
Save-Status([StatusType]::ubuntuUpdated)
### STATUS ubuntuDesktopInstalled ###
Log -Msg "....Instalando entorno grafico"
wsl sh -c "/mnt/c/users/$env:userName/.ubuntu/linux_cmds.sh install_gui"
# if ($result -Match "ERROR") {
#   Log -Msg "$result" -Level "Error"
#   Exit
# } else {
#   Save-Status([StatusType]::ubuntuDesktopInstalled)
# }
Save-Status([StatusType]::ubuntuDesktopInstalled)

Log -Msg "....Creando usuario Ceibal"
$result=wsl sh -c "/mnt/c/users/$env:userName/.ubuntu/linux_cmds.sh user_ceibal"
if ($result -Match "ERROR") {
  Log -Msg "$result" -Level "Error"
  Exit
}

$result=wsl sh -c "/mnt/c/users/$env:userName/.ubuntu/linux_cmds.sh set_username"
if ($result -Match "ERROR") {
  Log -Msg "$result" -Level "Error"
  Exit
}

Ubuntu2004.exe config --default-user ceibal  | Log -Msg "Configuro ceibal como usuario por defecto ..."


$shortcut_location = "$env:userprofile\Desktop\Ubuntu.lnk"
$program_location = "$env:userprofile\.ubuntu\03_start_mate.vbs"
$icon_location = "$env:userprofile\.ubuntu\ubuntu.ico"

$object = new-object -comobject wscript.shell
$shortcut = $object.createshortcut($shortcut_location)
$shortcut.targetpath = $program_location
$shortcut.iconlocation = $icon_location
$shortcut.save() *>&1 | Log -Msg "....Acceso directo creado"

New-NetFirewallRule -DisplayName "Xserver" -Direction Inbound -Program "$env:ProgramFiles\VcXsrv\vcxsrv.exe" -Action Allow *>&1| Log -Msg "....Agrego regla en Firewall"

wsl --shutdown *>&1 | Log -Msg "Reinicio wsl"

### STATUS complete ###
Save-Status([StatusType]::complete)
Log -Msg ""
Log -Msg "## Fin de la configuracion del entorno grafico ##"

Log -Msg ""
Log -Msg ""
Log -Msg "#### Fin instalacion de Ubuntu ####"
