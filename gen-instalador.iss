
[Setup]
AppId={{F679B93B-8F44-489B-B523-66874C87B3E4}
AppName=Instalador Ubuntu
AppVersion=1.7
;AppVerName=Instalador Ubuntu 1.5
AppPublisher=Plan Ceibal.
AppPublisherURL=https://www.ceibal.edu.uy/
AppSupportURL=https://www.ceibal.edu.uy/
AppUpdatesURL=https://www.ceibal.edu.uy/
DefaultDirName={autopf}\Instalador Ubuntu
DisableProgramGroupPage=yes
; Uncomment the following line to run in non administrative install mode (install for current user only.)
;PrivilegesRequired=lowest
OutputBaseFilename=Instalador de Ubuntu
SetupIconFile=ubuntu.ico
Compression=lzma
SolidCompression=yes
WizardStyle=modern
WizardSizePercent=150,150

[Languages]
Name: "spanish"; MessagesFile: "compiler:\Languages\Spanish.isl"
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
;Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "installer\install.exe"; Flags:dontcopy;
Source: "installer\setupdll.dll"; Flags:dontcopy;


[Code]

var
  Page: TOutputMsgMemoWizardPage;
  logSL: TStrings;
  ExecutionCompleteFlag: Boolean;
  cmdline: String;

function MyExec(const ACommandLine: String; ALogMsg: TStrings): Cardinal; external 'Exec@files:setupdll.dll stdcall delayload';
function SignalRestart: Boolean; external 'SignalRestart@files:setupdll.dll stdcall delayload';

procedure InitializeWizard();
begin
  Page := CreateOutputMsgMemoPage(wpReady,
   'Informacion', '',
   'Haga click en Next para iniciar la instalacion',
   '');

  Page.RichEditViewer.UseRichEdit := False;
  logSL := Page.RichEditViewer.Lines;
  ExecutionCompleteFlag := False;

  cmdline := ExpandConstant('{tmp}\install.exe');
end;

procedure CurPageChanged(CurPageID: Integer);
begin
   if ((not ExecutionCompleteFlag) and (CurPageID = Page.ID)) then
   begin
     ExtractTemporaryFile('install.exe');
     MyExec(cmdline, logSL);
     ExecutionCompleteFlag := True;
   end
end;

procedure CurStepChanged(CurStep: TSetupStep);
var
 intResultCode: Integer;
begin
  if CurStep = ssDone then
  begin
    if(FileExists(GetTempDir + 'reboot.TMP')) then
        if SuppressibleMsgBox('Debe reiniciar el PC para completar la instalación.' + #13#10 + #13#10 +
            'Desea reiniciar ahora ?',
            mbConfirmation, MB_YESNO, IDYES) = IDYES then
            begin
              Exec('shutdown.exe', '-r -t 0', '', SW_HIDE,
                ewNoWait, intResultCode);
              RegWriteStringValue(HKEY_CURRENT_USER, 'Software\Microsoft\Windows\CurrentVersion\RunOnce',
                'ResumeSetup', ExpandConstant('{srcexe}'));
            end;
  end;
end;