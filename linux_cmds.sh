#!/bin/bash
function spiner () {
  spin='-+*#'
  i=0
  while kill -0 $1 &> /dev/null;
  do
        i=$(( (i+1) %4 ))
        printf "\r${spin:$i:1}"
        sleep 1
  done
  echo ""
}




if [[ "$1" == "set_username" ]]
then
  sed -i 's/USER_WIN/'"$(wslvar USERNAME)"'/g' /mnt/c/users/$(wslvar USERNAME)/.ubuntu/03_start_mate.vbs > /dev/null 2> err
  cat err
fi

if [[ "$1" == "update_pkgs" ]]
then
  exec sh -c "export DEBIAN_FRONTEND=noninteractive && apt-get update && apt --fix-broken install && dpkg --configure -a && apt upgrade -y" > /dev/null 2> err  &
  spiner $!
  cat err
fi

if [[ "$1" == "install_gui" ]]
then
  exec sh -c "export DEBIAN_FRONTEND=noninteractive && apt --fix-broken install && dpkg --configure -a && apt install -y ubuntu-mate-core ubuntu-mate-desktop" > /dev/null 2> err &
  spiner $!
  cat err
fi

if [[ "$1" == "user_ceibal" ]]
then
  useradd -m -s /bin/bash ceibal && echo 'ceibal:ceibal' | chpasswd; usermod -aG sudo ceibal > /dev/null 2> err
  echo 'ceibal ALL=(ALL) NOPASSWD:ALL' |  EDITOR='tee' visudo --file /etc/sudoers.d/ceibal > /dev/null 2>> err
  cat err
fi

